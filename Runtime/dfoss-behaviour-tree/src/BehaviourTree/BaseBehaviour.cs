﻿using System;

namespace BehaviourTree
{
    public abstract class BaseBehaviour<TContext> : IBehaviour<TContext>
    {
        public string Name { get; }
        public BehaviourStatus Status { get; private set; } = BehaviourStatus.Ready;

        protected BaseBehaviour(string name)
        {
            Name = name;
        }

        public BehaviourStatus Tick(TContext context)
        {
            if (Status == BehaviourStatus.Ready)
            {
                OnInitialize(context);
            }

            Status = Update(context);

            if (Status == BehaviourStatus.Ready)
            {
                throw new InvalidOperationException("Ready status should not be returned by Behaviour Update Method");
            }

            if (Status != BehaviourStatus.Running)
            {
                OnTerminate(context, Status);
            }

            return Status;
        }

        public void Reset(TContext context)
        {
            if (Status == BehaviourStatus.Ready)
            {
                return;
            }

            DoReset(context, Status);
            Status = BehaviourStatus.Ready;
        }

        protected abstract BehaviourStatus Update(TContext context);

        protected virtual void OnTerminate(TContext context, BehaviourStatus status)
        {
        }

        protected virtual void OnInitialize(TContext context)
        {
        }

        protected virtual void DoReset(TContext context, BehaviourStatus status)
        {
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}